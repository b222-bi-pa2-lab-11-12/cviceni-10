# Lambda funkce

Lambda funkce jsou anonymní funkce (nemají jméno), lze je vytvářet a používat ad hoc - lze je předávat jako parametry mnoha algoritmů, které vyžadují nějakou funkci, aniž by bylo potřeba deklarovat funkci.

## Základní použití

Definice lambda funkce v C++ je jednoduchá, stačí napsat dvojici hranatých závorek, dále už následuje klasicky hlavička a tělo; návratový typ si odvodí kompilátor automaticky z typu výrazu v `return`, popřípadě mu lze napovědět pomocí -> TYP

Příklad:

```cpp
std::vector<int> v;
// read numbers from input
int number;
while (std::cin >> number) {
    v.push_back(number);
}
// sort numbers by their evenness - evens first, then odds
std::sort(v.begin(), v.end(), [](int a, int b) {
    return (a % 2) < (b % 2);
});
// write numbers to output
for (auto i : v) {
    std::cout << i << " ";
}
```

Příklad nápovědy typu
    
```cpp

auto loadInt = [] (std::istream & is) -> int * {
    int i;
    if(!(is >> i)) return nullptr;
    return new int(i);
};
```
Zde by kompilátor bez nápovědy hlásil chybu jelikož není jednoznačné co funkce vrací


Pokud hranaté závorky uvozující lambda funkci zůstanou prázdné, zkompiluje se lambda funkce stejně jako jakákoliv jiná funkce v C/C++. A lze jí následně uložit do ukazatele na funkci a volat ji přes něj.


Do hranatých závorek však můžeme i napsat proměnné, které si bereme z vnějšího bloku. Kompilátor potom místo obyčejné funkce vygeneruje funktor, a proměnné uvedené v hranatých závorkách se stanou jeho členskými proměnnými; zároveň obsah hranatých závorek (kterému se říká zachycení) lze považovat za konstruktor funktoru.

V zachycení můžeme:
* Kopírovat obsah proměnné, např.: `[i]`
* Vzít si referenci na proměnnou, např.: `[&i]`
* Zkopírovat si všechny použité proměnné: `[=]`
* Vzít si reference na všechny použité proměnné: `[&]`
* Vzít si pointer na aktuální objekt: `[this]`
* Zkopírovat si aktuální objekt: `[*this]`

A pár dalších možností, viz zde: https://en.cppreference.com/w/cpp/language/lambda#Lambda_capture

Příklad se zachycením:

```cpp
// read numbers from input
std::vector<int> v;
// read numbers from input
int number;
while (std::cin >> number) {
v.push_back(number);
}
// sort numbers, but we want to count comparisons
int count;
std::sort(v.begin(), v.end(), [&count](int a, int b) {
    count++;
    return a < b; 
});    
// write statistics
std::cout << "Sorted " << v.size() << " elements using " << count << " comparisons." << std::endl;
// write numbers to output
for (auto i : v) {
std::cout << i << " ";
}
```

## Uložení pomocí `std::function`

Jak si mohu lambda výraz uložit pro pozdější použití?

* Mohu využít pointer na funkci, pokud lambda výraz nemá zachycení.
* Mohu si napsat vlastní šablonu řídící se typem lambda výrazu.
* Mohu využít šablonu `std::function` z hlavičkového souboru `<functional>`, která dělá předchozí bod za mě.

Zápis pointeru na funkci lze použít jen někdy, navíc v syntaxi jazyka C nebývá příliš přehledný. Nejlepší je třetí varianta s `std::function`. Obalí jak funktor, tak pointer na funkci, navíc se sám tváří jako funktor a lze jej používat jako typový parametr pro některé kontejnery.

Příklad, ve kterém lambda výraz použijeme jako komparátor pro prvky v množině.

```cpp
// synonyms for long type names
using stringComparer = std::function<bool(const std::string &, const std::string &)>;
// fill set of strings with words from input; we will ignore case
std::set<std::string, stringComparer> strings([](const std::string & a, const std::string & b) {
    return strcasecmp(a.c_str(), b.c_str()) < 0;
});

std::string word;
while (std::cin >> word) {
    strings.insert(word);
}
// print set of strings     
for (auto i : strings) {
std::cout << i << std::endl;
}
```


# Dnešní cviko
Klasický `git clone https://gitlab.fit.cvut.cz/b222-bi-pa2-lab-11-12/cviceni-10.git` následně si otevřete složku `cviceni-10` a nejlépe načtěte CMakelist.txt jako projekt do CLionu.

Seznamte se předpřipravenými třídy CShape které byli obohaceny o equal metodu samozřejmně můžete toto rohraní měnit a pro jeden pod ukol to bude i potřeba.

Koukněte se na soubor in.txt který obsahuje možný návrh ukládání tvarů (můžete použít i vlastní).

## Úkol 1
Viz kapitola Aplikace lambda výrazů, použijte jednu z doporučených metod na načtení tvarů ze souboru in.txt nebo z vstupu od uživatele.

## Úkol 2
Uložte tvary do vektoru a následně je vypište.

## Úkol X
Ukládejte pouze unikátní tvary.

## Úkol X
Seřaďte tvary podle jejich obsahu nebo jiné vlastnosti z vašeho výběru.

## Úkol X
Upravte výpis aby obsahoval jejich náčrtek nebo alespoň nějakou jinou konkrátní informaci o tvaru (rovnoramenný trojúhelník, čtverec, název tvaru)

## Úkol X
Přidejte možnost kopírování tvarů.

# Aplikace lambda výrazů

Minule jsme u tvarů nestihli přidat možnost načítání ze vstupu. K tomu by se hodilo mít nějakou funkci, která dostane vstup, na základě kterého vytvoří konkrétní instanci některého z potomků, podle toho, co je na vstupu. Tuto instanci bude muset samozřejmě vrátit jako pointer. (Může být i chytrý, např. `std::unique_ptr`)

Potřebujeme tedy:

```cpp
CShape * loadShape(std::istream & in) {
    // ...
}
```
Popřípadě chytrý pointer:

```cpp
std::unique_ptr<CShape> loadShape(std::istream & in) {
    // ...
}
```

Ale co bude uvnitř takové funkce?
Nejspíš něco jako:

```cpp
std::string type;
if(!(in >> type)) return nullptr;
if(type == "[C]") {
    return new CCircle(in);
}
else if(type == "[R]") {
    return new CRectangle(in);
}
else if(type == "[T]") {
    return new CTriangle(in);
}
else {
    return nullptr;
}
```

Takový dlouhý vlak ifů však není přehledný, navíc je pomalý, protože hledá správnou variantu lineárně. Určitě existuje lepší způsob. Co kdybychom použili mapu ze stringů na funkci (lambdu)?

```cpp
static const std::map<std::string, std::function<std::unique_ptr<CShape> (std::istream &)>> factories = {
    { "[C]", [](std::istream & in) { ... } },
    { "[R]", [](std::istream & in) { ... } },
    { "[T]", [](std::istream & in) { ... } },
}; 

std::string type;
if(!(in >> type)) return nullptr;
auto it = factories.find(type);
if(it == factories.end()) return nullptr;
return it->second(in);
```

To vypadá (o něco) lépe, navíc díky mapě hledáme správnou variantu binárním vyhledáváním, což je rychlejší než vláček ifů.


https://gitlab.fit.cvut.cz/matouj10/pa2-2022-lab/-/tree/master/lab/09/1-lambda
