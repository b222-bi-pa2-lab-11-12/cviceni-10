#ifndef LAMBDY_CSHAPE_H
#define LAMBDY_CSHAPE_H

#include <ostream>

const double PRECISION = 1e-6;

bool nearly_equal(double a, double b);

bool smaller_or_equal(double a, double b);

class CShape {
public:
    virtual double getCircumference() const = 0;
    virtual double getContent() const = 0;

    virtual bool equals(const CShape &rhs) const = 0;

    virtual ~CShape() = default;
};

std::ostream & operator << (std::ostream &os, const CShape &shape);

#endif //LAMBDY_CSHAPE_H
