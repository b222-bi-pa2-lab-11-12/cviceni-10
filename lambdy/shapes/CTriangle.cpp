#include <cmath>
#include <vector>
#include <algorithm>
#include "CTriangle.h"

double CTriangle::getCircumference() const {
    return a + b + c;
}

double CTriangle::getContent() const {
    // https://en.wikipedia.org/wiki/Heron%27s_formula
    double s = (a + b + c) / 2;
    return sqrt(s * (s - a) * (s - b) * (s - c));
}

bool CTriangle::equals(const CShape &rhs) const {
    const CTriangle *triangle = dynamic_cast<const CTriangle *>(&rhs);
    if (triangle != nullptr) {
        std::vector<double> sides = {a, b, c};
        std::vector<double> rhsSides = {triangle->a, triangle->b, triangle->c};
        std::sort(sides.begin(), sides.end());
        std::sort(rhsSides.begin(), rhsSides.end());
        return sides == rhsSides;
    }
    return false;
}

CTriangle::CTriangle(double a, double b, double c) : a(a), b(b), c(c) {
    if (a + b <= c || a + c <= b || b + c <= a || a <= 0 || b <= 0 || c <= 0)
        throw std::invalid_argument("Invalid triangle");
}