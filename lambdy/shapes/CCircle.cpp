#include <stdexcept>
#include <cmath>
#include "CCircle.h"

CCircle::CCircle(double r) : r(r) {
    if (r <= 0)
        throw std::invalid_argument("Invalid rectangle");
}

double CCircle::getCircumference() const {
    return 2 * M_PI * r;
}

double CCircle::getContent() const {
    return M_PI * r * r;
}

bool CCircle::equals(const CShape &rhs) const {
    const CCircle *circle = dynamic_cast<const CCircle *>(&rhs);
    if (circle != nullptr) {
        return nearly_equal(r, circle->r);
    }
    return false;
}