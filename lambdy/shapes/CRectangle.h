#ifndef LAMBDY_CRECTANGLE_H
#define LAMBDY_CRECTANGLE_H


#include <stdexcept>
#include "CShape.h"

class CRectangle : public CShape {
    double a, b;
public:

    CRectangle(double a, double b);

    double getCircumference() const override;

    double getContent() const override;

    bool equals(const CShape &rhs) const override;

};


#endif //LAMBDY_CRECTANGLE_H
