#ifndef LAMBDY_CCIRCLE_H
#define LAMBDY_CCIRCLE_H


#include "CShape.h"

class CCircle : public CShape {
    double r;
public:
        explicit CCircle(double r);

        double getCircumference() const override;

        double getContent() const override;

        bool equals(const CShape &rhs) const override;

};


#endif //LAMBDY_CCIRCLE_H
