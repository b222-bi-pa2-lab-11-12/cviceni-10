#ifndef LAMBDY_CTRIANGLE_H
#define LAMBDY_CTRIANGLE_H


#include <stdexcept>
#include "CShape.h"

class CTriangle : public CShape {
    double a, b, c;
public:

    CTriangle(double a, double b, double c);

    double getCircumference() const override;

    double getContent() const override;

    bool equals(const CShape &rhs) const override;

};


#endif //LAMBDY_CTRIANGLE_H
