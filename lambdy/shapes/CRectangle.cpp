#include <algorithm>
#include <vector>
#include "CRectangle.h"

CRectangle::CRectangle(double a, double b) : a(a), b(b) {
    if (a <= 0 || b <= 0)
        throw std::invalid_argument("Invalid rectangle");
}

double CRectangle::getCircumference() const {
    return (a + b) * 2;
}

double CRectangle::getContent() const {
    return a * b;
}

bool CRectangle::equals(const CShape &rhs) const {
    const CRectangle *rectangle = dynamic_cast<const CRectangle *>(&rhs);
    if (rectangle != nullptr) {
        std::vector<double> sides = {a, b};
        std::vector<double> rhsSides = {rectangle->a, rectangle->b};
        std::sort(sides.begin(), sides.end());
        std::sort(rhsSides.begin(), rhsSides.end());
        return sides == rhsSides;
    }
    return false;
}

