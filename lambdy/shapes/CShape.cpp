#include "CShape.h"
#include <cmath>

bool nearly_equal(double a, double b) {
    return std::abs(a - b) <= (PRECISION * std::abs(a) * std::abs(b));
}

bool smaller_or_equal(double a, double b) {
    return a < b || nearly_equal(a, b);
}

std::ostream &operator<<(std::ostream &os, const CShape &shape) {
    os << "Circumference: " << shape.getCircumference() << ", Content: " << shape.getContent();
    // TODO: More specific printing
    // HINT: shape.print(os); virtual method
    return os;
}
