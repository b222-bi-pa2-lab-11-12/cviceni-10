#include <iostream>
#include <algorithm>
#include <iterator>
#include <vector>
#include <memory>
#include <map>
#include <functional>
#include <fstream>
#include "shapes/CShape.h"
#include "shapes/CCircle.h"
#include "shapes/CRectangle.h"
#include "shapes/CTriangle.h"

std::unique_ptr<CShape> loadShape(std::istream & input = std::cin) {
    // TODO: implement
    return nullptr;
}


int main() {
    std::vector<std::unique_ptr<CShape>> shapes;
    std::unique_ptr<CShape> shape;
    do {
        shape = loadShape();
        // TODO: Add shape to collection
        // BONUS: Add only unique shapes
        // BONUS2: Sort them by one of the properties (lambda)
    } while (shape); // TODO: mistake, why?

    for(const auto & shape : shapes) {
        std::cout << *shape << std::endl;
    }
}
