#include <iostream>
#include <algorithm>
#include <iterator>
#include <vector>
#include <functional>
#include <set>
#include <cstring>

int main() {
    std::vector<int> v;

    // read numbers from input
    int number;
    while (std::cin >> number) {
        v.push_back(number);
    }

    // sort numbers by their evenness - evens first, then odds
    std::sort(v.begin(), v.end(), [](int a, int b) {
        return (a % 2) < (b % 2);
    });

    // write numbers to output
    for (auto i : v) {
        std::cout << i << " ";
    }
}
